# PIHATPROJECT

PiHAT project showing the design of a UPS 
Our pi hat serves as a backup power supply to our rasberry pi 
it can be used to power drones or small computers to save data before switch off.

user instrunctions 

 User role/Scenario 1
User who is using a Raspberry Pi for surveillance purposes in an area that experiences random power cuts
• A UPS HAT that provides enough power to shut down the Pi long enough to safely store the surveillance data
• UPS HAT with a rechargeable power system, that recharges as the raspberry Pi is using power from main supply
• A UPS that regulates the voltage spikes and dips the main power supply User role/Scenario 2
User using Raspberry Pi to fly a drone
• A UPS that powers up the raspberry pi long enough to fly the drone for a period of time.
• Small and portable to travel with to different locations to fly the drone.
• Light weight so it doesn’t weigh down the drone.
• A UPS with rechargeable batteries.
User role/Scenario 3
User who lives in a remote location
• A UPS HAT that provides enough power supply to use the Raspberry pi for a period
• A UPS HAT that can be recharged and used repeatedly.
• To protect device from surges, dips and failures.
